module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        const Block = require('./block')
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            let newBlock = msg.payload;

            let result =
            {
                success: false,
                msg: "Unknown Error",
                block: newBlock
            }

            if(typeof newBlock == "string")
            {
                newBlock = JSON.parse(newBlock);
            }

            if(!newBlock.timestamp)
            {
                result.msg = "Block is missing timestamp";
                
                msg.payload = result;
                node.send(msg);
                return;
            }

            if(!newBlock.previousHash)
            {
                result.msg = "Block is missing previousHash";
                
                msg.payload = result;
                node.send(msg);
                return;
            }

            if(newBlock.nonce === null)
            {
                result.msg = "Block is missing nonce";
                
                msg.payload = result;
                node.send(msg);
                return;
            }


            let bl = new Block(newBlock.timestamp, newBlock.data, newBlock.previousHash);
            bl.nonce = newBlock.nonce;
            bl.hash = bl.calculateHash();

            newBlock = bl;

            let prevBlock = this.context().flow.sense_blockchain.getLatestBlock();

            if(prevBlock.hash == newBlock.previousHash)
            {
                if(newBlock.hash == newBlock.calculateHash())
                {
                    if(this.context().flow.sense_blockchain.addBlock(newBlock))
                    {
                        result.success = true;
                        result.block = newBlock;
                        result.msg = "Block successfully added!"
                        
                        msg.payload = result;
                        node.send(msg);
                    }
                    else
                    {
                        result.msg = "Rejected by chain!";
                    
                        msg.payload = result;
                        node.send(msg);
                    }
                }
                else
                {
                    result.msg = "Hash invalid! is: [" + newBlock.hash + "]. Should be: [" + newBlock.calculateHash() + "]";
                
                    msg.payload = result;
                    node.send(msg);
                }
            }
            else
            {
                result.msg = "previousHash does not match hash of previous block.";
                
                msg.payload = result;
                node.send(msg);
                return;
            }
        });
    }

    RED.nodes.registerType("add-block", Blockchain);
}