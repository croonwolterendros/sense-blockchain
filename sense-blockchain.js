module.exports = function(RED) 
{
    function Blockchain_Init(config) 
    {
        const BlockChain = require ('./blockchain');

        RED.nodes.createNode(this, config);
        var node = this;
        
        this.context().flow.sense_blockchain = new BlockChain(config.difficultykey);
        
        node.on('input', function(msg) 
        {
            msg.payload = this.context().flow.sense_blockchain.chain;
            node.send(msg);
        });
    }

    RED.nodes.registerType("blockchain", Blockchain_Init);
}

/*
TODO:

- Add block to chain via extra node
- Ensure only one chain is created regardless of the amount of chain nodes (maybe done via error)
- Save chain to file
- Load chain from file
- Mine block and output
- Add block format to help so users can load external blocks

- Basically wrap a node around each Blockchain function?
*/