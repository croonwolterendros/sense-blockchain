This package contains three nodes for use in Node-RED to add simple blockchain functionality to your flows.

There are six nodes in this package:

- Blockchain:
This node can be used to configure the chain. Ensure a single one is present on your flow (not more than one!)
Inside this node you can set the Difficulty Key for blocks in the chain. The block inside will only be accepted if the SHA265-hash begins with these characters.

- Mine Block:
This node will mine a new block with the data from msg.payload. The use of this block inside your flow is not recommended as it blocks your flow from executing anything else while it mines, but it can be used for testing purposes. The output will be your new block as a JSON object, or as a JSON string.

- Add Block:
This node will attempt to insert the block input via msg.payload to the chain.

- Chain to JSON:
This node will, when triggered via the input, output the entire chain as JSON object.

- Latest Block:
This node will, when triggered via the input, output the last block in the chain as JSON object.

- Parse JSON:
This node will parse a blockchain in JSON format and load it into the current flow. Use this to load previously saved chains.

- SHA265 Hash::
Each block has a hash. In order to add a block to the chain, this hash must be calculated correctly.
The hash is calculated as follows:

str = this.previousHash.toString() + this.timestamp.toString() + this.data.toString() + this.nonce.toString();
hash = str as SHA265 hash, output as Base64 string.

- Block structure:
Each block is a JSON object containing the following info:


{
    timestamp: number (milliseconds since 1-1-1970)
    data: anything
    previousHash: hash of previous block
    nonce: number
    hash: valid SHA256 hash of this block
}
