module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        const Block = require('./block')
        const BlockChain = require('./blockchain')
        
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            let obj = typeof msg.payload === 'string'? JSON.parse(msg.payload) : msg.payload;

            let result =
            {
                success: false,
                msg: "Unknown Error"
            }

            let newChain = new BlockChain();

            if(!obj.difficultyKey)
            {
                result.msg = "String is not a valid JSON BlockChain: Missing difficultyKey."

                msg.payload = result;
                node.send(msg);
                return;
            }

            if(!obj.genesis)
            {
                result.msg = "String is not a valid JSON BlockChain: Missing genesis."

                msg.payload = result;
                node.send(msg);
                return;
            }

            if(!obj.chain)
            {
                result.msg = "String is not a valid JSON BlockChain: Missing chain."
                
                msg.payload = result;
                node.send(msg);
                return;
            }

            newChain.difficultyKey = obj.difficultyKey;
            newChain.chain = obj.chain;
            newChain.genesis = obj.genesis;

            for(let i = 0; i < newChain.chain.length; i++)
            {
                if(!newChain.chain[i].timestamp)
                {
                    result.msg = "String is not a valid JSON BlockChain: Block[" + i + "] is missing timestamp";
                    
                    msg.payload = result;
                    node.send(msg);
                    return;
                }

                if(!newChain.chain[i].previousHash && i > 0)
                {
                    result.msg = "String is not a valid JSON BlockChain: Block[" + i + "] is missing previousHash";
                    
                    msg.payload = result;
                    node.send(msg);
                    return;
                }

                if(newChain.chain[i].nonce === null)
                {
                    result.msg = "String is not a valid JSON BlockChain: Block[" + i + "] is missing nonce";
                    
                    msg.payload = result;
                    node.send(msg);
                    return;
                }


                let bl = new Block(newChain.chain[i].timestamp, newChain.chain[i].data, newChain.chain[i].previousHash);
                bl.nonce = newChain.chain[i].nonce;

                bl.hash = bl.calculateHash();

                newChain.chain[i] = bl;
            }
            
            if(newChain.isChainValid())
            {
                result.success = true;
                result.msg = "Chain successfully parsed and initialized."

                node.context().flow.sense_blockchain = newChain;

                msg.payload = result;
                node.send(msg);
            }
            else
            {
                result.msg = "String is not a valid JSON BlockChain: Chain invalid.";
                
                msg.payload = result;
                node.send(msg);
                return;
            }
        });
    }

    RED.nodes.registerType("parse-json", Blockchain);
}