"use strict"; 

const Block = require('./block')

class BlockChain
{
    constructor(diff = '123')
    {
        let now = Date.now()
        
        this.genesis = now;
        this.difficultyKey = diff;
        this.chain = [this.createGenesisBlock()];
    }

    isChainValid()
    {
        for(let i = 1; i < this.chain.length; i++)
        {
            if(this.chain[i].hash !== this.chain[i].calculateHash())
                return false;

            if(this.chain[i].previousHash !== this.chain[i-1].hash)
                return false;
        }
           
        if(this.chain[0].calculateHash() !== this.createGenesisBlock().calculateHash())
            return false;

        return true;
    }

    getLatestBlock()
    {
        return this.chain[this.chain.length - 1];
    }

    addBlock(newBlock)
    {
        newBlock.previousHash = this.getLatestBlock().hash;

        newBlock.mineBlock(this.difficultyKey);

        if(this.getLatestBlock().hash === newBlock.previousHash)
        {   
            this.chain.push(newBlock);
            return true;
        }

        return false;
    }

    createGenesisBlock()
    {
        let newBlock = new Block(this.genesis, "Genesis Block", "0");
        newBlock.calculateHash();
        return newBlock;
    }

    static getDateStr()
    {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let MM = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        let hh = String(today.getHours()).padStart(2, '0');
        let mm = String(today.getMinutes()).padStart(2, '0');

        return dd + '-' + MM + '-' + yyyy + ", " + hh + ":" + mm;
    }
}

module.exports = BlockChain;