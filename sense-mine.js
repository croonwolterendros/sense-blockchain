module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        const Block = require('./block')
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            let result =
            {
                success: false,
                msg: "Unknown Error",
                block: null
            }

            if(this.context().flow.sense_blockchain !== null)
            {
                let latest = this.context().flow.sense_blockchain.getLatestBlock();
                
                let newBlock = new Block(Date.now(), msg.payload, latest.hash);
                let th = this;

                newBlock.mineBlock(this.context().flow.sense_blockchain.difficultyKey, function()
                {
                    let str = JSON.stringify(newBlock);
                    result = config.outputtype === "string"? str : JSON.parse(str);
                });
            }
            else
            {
                result.msg = "The blockchain was not yet initialized. Please ensure a sense blockchain node is present on this flow.";
                throw "The blockchain was not yet initialized. Please ensure a sense blockchain node is present on this flow."
            }

            msg.payload = result;
            node.send(msg);
        });
    }

    RED.nodes.registerType("mine-block", Blockchain);
}