module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        const Block = require('./block')
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            let str = JSON.stringify(node.context().flow.sense_blockchain);
            msg.payload = config.outputtype === "string"? str : JSON.parse(str);
            node.send(msg);
        });
    }

    RED.nodes.registerType("chain-to-json", Blockchain);
}