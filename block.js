const crypto = require('crypto');

class Block
{
    constructor(timestamp, data, previousHash = '')
    {
        this.timestamp = timestamp;
        this.data = data;
        this.previousHash = previousHash;

        this.nonce = 0;

        this.hash = this.calculateHash();
    }

    mineBlock(difficulty, onComplete)
    {
        while(this.hash.substring(0, difficulty.length) !== difficulty)
        {
            this.nonce++;
            this.hash = this.calculateHash();
        }

        onComplete && onComplete();
    }

    calculateHash()
    {
        let hash = "";
        let str = this.previousHash.toString() + this.timestamp.toString() + this.data.toString() + this.nonce.toString();
        
        hash = crypto.createHash('sha256').update(str).digest('base64');

        return hash.toString()
    }
}

module.exports = Block;