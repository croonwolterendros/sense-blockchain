module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        const Block = require('./block')
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            let prevBlock = this.context().flow.sense_blockchain.getLatestBlock();
                
            let str = JSON.stringify(prevBlock);
            msg.payload = config.outputtype === "string"? str : JSON.parse(str);

            node.send(msg);
        });
    }

    RED.nodes.registerType("latest-block", Blockchain);
}