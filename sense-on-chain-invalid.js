module.exports = function(RED) 
{
    function Blockchain(config) 
    {
        RED.nodes.createNode(this, config);
        var node = this;

        setInterval(function()
        {
            if(node.context().flow.sense_blockchain)
            {
                if(!node.context().flow.sense_blockchain.isChainValid())
                {
                    let msg = {};
                    msg.payload = "Chain invalid!";
                    node.send(msg);
                }
            }
            else
            {
                let msg = {};
                msg.payload = "No chain detected."
                node.send(msg);
            }

        }, config.checkinterval)

        node.on('input', function(msg) 
        {
            
        });
    }

    RED.nodes.registerType("on-chain-invalid", Blockchain);
}